export const environment = {
    production: true,
    fireBase: {
        apiKey: 'AIzaSyD4L7mnqi9n2ccpqft1Fq996OjW0B79iEU',
        authDomain: 'looptime-cd3a9.firebaseapp.com',
        databaseURL: 'https://looptime-cd3a9.firebaseio.com',
        projectId: 'looptime-cd3a9',
        storageBucket: 'looptime-cd3a9.appspot.com',
        messagingSenderId: '497829188991',
    },
    appConfig: {
        yts_url: 'https://yts.am/api/v2/',
        eztv_url: 'https://eztv.io/api/get-torrents?imdb_id=',
        Popcorn_url: 'https://tv-v2.api-fetch.website',
        movieDB: {
            url: 'https://api.themoviedb.org/3',
            key: 'c3a07ff98aaeb2065ebee321bf08d23a',
        },
        fanart: {
            url: 'http://webservice.fanart.tv/v3',
            key: '801f3cd00477b961043fdecf4f41dd59',
            key1: '9835c7b65e0db4c3c367c14c9b596483',
        },
    },
}
