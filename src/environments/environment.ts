// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: true,
    fireBase: {
        apiKey: 'AIzaSyD4L7mnqi9n2ccpqft1Fq996OjW0B79iEU',
        authDomain: 'looptime-cd3a9.firebaseapp.com',
        databaseURL: 'https://looptime-cd3a9.firebaseio.com',
        projectId: 'looptime-cd3a9',
        storageBucket: 'looptime-cd3a9.appspot.com',
        messagingSenderId: '497829188991',
    },
    appConfig: {
        yts_url: 'https://yts.am/api/v2/',
        eztv_url: 'https://eztv.io/api/get-torrents?imdb_id=',
        Popcorn_url: 'https://tv-v2.api-fetch.website',
        movieDB: {
            url: 'https://api.themoviedb.org/3',
            key: 'c3a07ff98aaeb2065ebee321bf08d23a',
        },
        fanart: {
            url: 'http://webservice.fanart.tv/v3',
            key: '801f3cd00477b961043fdecf4f41dd59',
            key1: '9835c7b65e0db4c3c367c14c9b596483',
        },
    },
}

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

// Initialize Firebase
/* config = {
  apiKey: 'AIzaSyD49asLllFPN0f3k3TvQDY82uNFPiONVb0',
  authDomain: 'new-app-ef13a.firebaseapp.com',
  databaseURL: 'https://new-app-ef13a.firebaseio.com',
  projectId: 'new-app-ef13a',
  storageBucket: '',
  messagingSenderId: '419425700702'
}; */
