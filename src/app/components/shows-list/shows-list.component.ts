import {
    Component,
    OnInit,
    ViewChild,
    OnDestroy,
    AfterViewInit,
    ElementRef,
} from '@angular/core'
import { SearchService } from '../../services/search.service'
import { ShowDetailsComponent } from '../show-details/show-details.component'
import { UiServiceService } from '../../services/ui-service.service'
import { Subscription, fromEvent } from 'rxjs'
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators'
import { AppStateService } from '../../services/app-state.service'
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling'
import { NgAnimateScrollService } from 'ng-animate-scroll'

@Component({
    selector: 'app-shows-list',
    templateUrl: './shows-list.component.html',
    styleUrls: ['./shows-list.component.scss'],
})
export class ShowsListComponent implements OnInit, OnDestroy, AfterViewInit {
    public Shows: Array<any> = []
    public Pages: any
    errorState: boolean
    page: number = 1
    showsLoading: boolean
    searchTerm

    subscribe: Subscription
    @ViewChild(CdkVirtualScrollViewport) cdkViewport: CdkVirtualScrollViewport
    @ViewChild('input') searchInput: ElementRef
    /** PAGINATION */
    // length = 2504
    // pageSize = 50
    // pageIndex
    // pageSizeOptions = [50, 30, 10]

    constructor(
        private UI: UiServiceService,
        private request: SearchService,
        private State: AppStateService,
        private animateScroll: NgAnimateScrollService
    ) {}

    ngOnInit() {
        this.requestShowList(this.page)
    }

    ngAfterViewInit() {
        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(2000),
                map((event: Event) => (<HTMLInputElement>event.target).value),
                distinctUntilChanged()
            )
            .subscribe((val: string) => {
                if (val.trim().length === 0 || !val) {
                    this.openSnackbar(`Can't Search of Nothing`, 2000)
                } else if (val.length === 0) {
                    // this.requestShowList(this.page)
                } else {
                    this.search(val)
                }
            })
    }

    requestShowList(pageNumber: number) {
        this.showsLoading = true
        this.subscribe = this.request.getShowsList(pageNumber).subscribe(
            data => {
                this.page++
                this.Shows = [...this.Shows, ...data]
                this.showsLoading = false
                this.errorState = false
            },
            err => {
                this.openSnackbar(err)
                this.errorState = true
                this.showsLoading = false
            }
        )
    }

    ScrollTop() {
        this.animateScroll.scrollToElement('TOP', 850, 'SCROLL')
    }

    openDialog(data): void {
        const info: object = {
            id: data,
        }
        this.UI.openDialog(
            info,
            ShowDetailsComponent,
            'Download-dialog',
            '100vh',
            '100vw'
        )
    }

    search(keyword: string) {
        this.ScrollTop()
        this.showsLoading = true
        this.errorState = false

        this.subscribe = this.request.getShowsByKeyword(keyword).subscribe(
            data => {
                this.Shows = [...data, ...this.Shows]
                this.showsLoading = false
                this.ScrollTop()
                if (data.length == 0) {
                    this.openSnackbar(`${keyword} Not Found`)
                } else {
                    this.openSnackbar(`${data.length} Result(s) Found`)
                }
            },
            err => {
                this.openSnackbar(err)
                this.showsLoading = false
                this.errorState = false
            }
        )
    }

    openSnackbar(msg: any, duration?: number) {
        this.UI.openSnackBar(msg, duration)
    }

    favorite(id) {
        console.log(id)
    }

    ngOnDestroy(): void {
        this.subscribe.unsubscribe()
        // this.State.TvShowsState.next(this.Shows)
    }
}
