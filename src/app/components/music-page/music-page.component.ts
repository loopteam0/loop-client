import { Component, OnInit, ViewChild } from '@angular/core'
import { MatPaginator } from '@angular/material'
import { MatSnackBar } from '@angular/material'
import { TorrentSearchApiService } from '../../services/torrent-search-api.service'
import { AppStateService } from '../../services/app-state.service'
import { UiServiceService } from '../../services/ui-service.service'

@Component({
    selector: `app-music-page`,
    templateUrl: `./music-page.component.html`,
    styleUrls: [`./music-page.component.scss`],
})
export class MusicPageComponent implements OnInit {
    loading: boolean
    Musics = this.State.MusicsListState
    searched: boolean
    errorState = false

    searchTerm = ''
    limit = 50
    category = 'PopularMusics'

    constructor(
        private Torrent: TorrentSearchApiService,
        private snackbar: MatSnackBar,
        private State: AppStateService,
        private UI: UiServiceService
    ) {}
    // dataSource = new MatTableDataSource<torrent>(this.Results);

    @ViewChild(MatPaginator)
    paginator: MatPaginator

    ngOnInit() {
        switch (this.Musics.value) {
            case null:
                this.showMusics()
                break
            case undefined:
                this.showMusics()
                break
            default:
                break
        }
    }

    async showMusics(query?: string) {
        this.searched = false
        this.loading = true
        this.errorState = false

        if (await query) {
            this.searched = true

            this.searchTerm = query
            // this.limit = 50
            this.category = 'Music'
        }

        this.Torrent.getTorrents(this.searchTerm, this.category, this.limit)
            .then(torrents => {
                this.State.MusicsListState.next(torrents)
                this.errorState = false

                this.loading = false
            })
            .catch(err => {
                this.UI.openSnackBar(err)
                this.loading = false
                this.errorState = true
            })
    }

    //  search(key){
    //    this.searched = true;
    //      this.loading = true;
    //      this.errorState = false;
    //   this.Torrent.getTorrents(key, 'Music', 50)
    //     .then(torrents => {
    //       this.Musics = torrents;
    //       this.loading = false;
    //       this.showError(`${this.Musics.length} Results Found On ${key}`);
    //     }, err => {
    //     this.showError(err);
    //     this.loading = false;
    //     });
    //  }

    download(torrent) {
        this.Torrent.downloadMagnet(torrent)
    }

    showError(err) {
        this.snackbar.open(err, null, {
            duration: 3000,
        })
    }
}

export interface torrent {
    title?: string
    size?: string
    seeds?: number
    peers?: number
    time?: string
    magnet?: string
    desc?: string
    provider?: string
}
