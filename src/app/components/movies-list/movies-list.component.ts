import {
    Component,
    OnInit,
    ViewChild,
    AfterViewInit,
    OnDestroy,
    ElementRef,
} from '@angular/core'
import { SearchService } from '../../services/search.service'
import { MovieDetailsComponent } from '../movie-details/movie-details.component'
import { UiServiceService } from '../../services/ui-service.service'
import { Subscription, fromEvent } from 'rxjs'
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators'
import { AppStateService } from '../../services/app-state.service'
import { NgAnimateScrollService } from 'ng-animate-scroll'

@Component({
    selector: 'app-movies-list',
    templateUrl: './movies-list.component.html',
    styleUrls: ['./movies-list.component.scss'],
})
export class MoviesListComponent implements OnInit, OnDestroy, AfterViewInit {
    public Movies: Array<any> = []
    public Pages: any
    errorState = false
    moviesLoading: boolean
    selectedValue: string
    background: any
    banner: any
    searchLt: any
    page = 1
    subscribe: Subscription
    @ViewChild('input') searchInput: ElementRef

    // pagination
    // length: number
    // pageSize = 50
    // pageIndex: any
    // pageSizeOptions = [50, 30, 10]
    // retryIndex: number
    // pagination: boolean = true

    constructor(
        public UI: UiServiceService,
        private request: SearchService,
        private State: AppStateService,
        private animateScroll: NgAnimateScrollService
    ) {}

    ngOnInit() {
        this.requestMoviesList(this.page)
    }

    ngAfterViewInit() {
        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(2000),
                distinctUntilChanged(),
                map((event: Event) => (<HTMLInputElement>event.target).value)
            )
            .subscribe((val: string) => {
                if (val.trim().length === 0 || !val) {
                    this.UI.openSnackBar("Can't search for Nothing")
                } else if (val.length === 0) {
                    /// TODO:
                } else {
                    this.search(val)
                }
            })
    }

    /** Get Movies List from Yts  the 'i' param is for pagination*/
    requestMoviesList(i: number) {
        this.moviesLoading = true
        this.errorState = false
        this.subscribe = this.request.getMoviesList(i, 50).subscribe(
            data => {
                this.page++
                this.Movies = [...this.Movies, ...data['movies']]
                // this.length = data['movie_count']
                this.moviesLoading = false
                this.errorState = false
            },
            err => {
                this.openSnackbar(err)
                this.errorState = true
                this.moviesLoading = false
            }
        )
    }

    search(keyword: string) {
        this.ScrollTop()
        this.moviesLoading = true
        this.errorState = false
        this.subscribe = this.request.getMoviesByKeyword(keyword).subscribe(
            data => {
                this.moviesLoading = false
                this.Movies = [...data['movies'], ...this.Movies]
                this.ScrollTop()
                // this.length = data['movie_count']
                if (data['movie_count'] == 0) {
                    this.openSnackbar(`Nothing Found`)
                } else {
                    this.openSnackbar(`${data['movie_count']} Result(s) Found`)
                }
            },
            err => {
                this.openSnackbar(err)
                this.moviesLoading = false
            }
        )
    }

    openDialog(id: any, imdb_id): void {
        const info: object = {
            id: id,
            imdb_id: imdb_id,
        }
        this.UI.openDialog(
            info,
            MovieDetailsComponent,
            'Download-dialog',
            '100%',
            '100vw'
        )
    }

    ScrollTop() {
        this.animateScroll.scrollToElement('TOP', 850, 'SCROLL')
    }

    openSnackbar(msg: string) {
        this.UI.openSnackBar(msg, 5000)
    }

    ngOnDestroy(): void {
        this.subscribe.unsubscribe()
        // this.State.MovieListState.next(this.Movies)
    }
}
