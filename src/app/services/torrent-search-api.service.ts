import { Injectable } from '@angular/core'
import { ElectronService } from './electron.service'
import { throwError } from 'rxjs'
import { UiServiceService } from './ui-service.service'

@Injectable({
    providedIn: 'root',
})
export class TorrentSearchApiService {
    constructor(
        private electron: ElectronService,
        private UI: UiServiceService
    ) {}

    //let torrents = await this.electron.TorrentSearch.search(['1337x', 'ThePirateBay','Torrent9'], title, cat , limit);

    async getTorrents(title: string, cat: string, limit: number) {
        this.electron.TorrentSearch.enableProvider('1337x')
        this.electron.TorrentSearch.enableProvider('ThePirateBay')
        // this.electron.TorrentSearch.enableProvider('Torrent9');
        let torrents = await this.electron.TorrentSearch.search(
            title,
            cat,
            limit
        )
        // console.log(torrents)

        if (torrents.length === 0) {
            return null
        } else {
            return torrents
        }
    }

    /* async pirateBayTop(cat){
      const searchResults = await this.PirateBay.topTorrents(cat);

      return searchResults;
     }

     async pirateBaySearch( keyword , cat){
      const searchResults = await this.PirateBay.search( keyword , {
        category: cat,
        orderBy: 'seeds',
        sortBy: 'desc'
      })
      return searchResults;
    } */

    async downloadTorrent(torrent) {
        return await this.electron.TorrentSearch.downloadTorrent(
            torrent,
            'C:\\Users\\shadow\\Downloads\\Compressed'
        )
    }

    /* download torrent */
    async downloadMagnet(torrent) {
        await this.electron.TorrentSearch.getMagnet(torrent)
            .then(magnet => {
                this.electron.shell.openExternal(magnet)
            })
            .catch(err => {
                this.UI.openSnackBar(err)
                // console.log(err)
            })
    }

    //    parser torrent virus

    async torrentParser(infoHash) {
        const uri = this.electron.torrentParser.toMagnetURI({
            infoHash,
        })
        // console.log(uri)

        this.electron.shell.openExternal(uri)
    }
}
