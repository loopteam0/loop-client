(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./node_modules/ng-animate-scroll/fesm5/ng-animate-scroll.js":
/*!*******************************************************************!*\
  !*** ./node_modules/ng-animate-scroll/fesm5/ng-animate-scroll.js ***!
  \*******************************************************************/
/*! exports provided: NgAnimateScrollService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgAnimateScrollService", function() { return NgAnimateScrollService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var NgAnimateScrollService = /** @class */ (function () {
    function NgAnimateScrollService() {
    }
    /**
     * @desc scrollToItem Fn scrolls to an items by utilising the animated scroll fn (scrollTo)
     *       and calculating the height of the header to accurately find the item's position.
     * @param elementID: element's ID that will be scrolled to.
     * @param duration: duration in milliseconds, default is 750.
     * @param container the container html native element (or its id), window will be used if not set
     */
    /**
     * @desc scrollToItem Fn scrolls to an items by utilising the animated scroll fn (scrollTo)
     *       and calculating the height of the header to accurately find the item's position.
     * @param {?} elementID
     * @param {?=} duration
     * @param {?=} container the container html native element (or its id), window will be used if not set
     * @return {?}
     */
    NgAnimateScrollService.prototype.scrollToElement = /**
     * @desc scrollToItem Fn scrolls to an items by utilising the animated scroll fn (scrollTo)
     *       and calculating the height of the header to accurately find the item's position.
     * @param {?} elementID
     * @param {?=} duration
     * @param {?=} container the container html native element (or its id), window will be used if not set
     * @return {?}
     */
    function (elementID, duration, container) {
        if (duration === void 0) { duration = 750; }
        /** @type {?} */
        var item = document.getElementById(elementID);
        if (item) {
            /** @type {?} */
            var itemPos = item.offsetTop;
            if (container) {
                if (typeof container === 'string') {
                    container = document.getElementById(container);
                }
                this.scrollTo(container, itemPos, duration, true);
            }
            else {
                this.scrollTo(window.document, itemPos, duration);
            }
        }
        else {
            console.error("Could not find element with the following ID: " + elementID);
        }
    };
    /**
     * @desc scrollTo Fn allows scrolling with animation.
     * @param element the 'element' that the scroll will happen on.
     * @param to is the location to scroll to.
     * @param duration is the length of the animation.
     */
    /**
     * @desc scrollTo Fn allows scrolling with animation.
     * @private
     * @param {?} element the 'element' that the scroll will happen on.
     * @param {?} to is the location to scroll to.
     * @param {?} duration is the length of the animation.
     * @param {?=} isContainer
     * @return {?}
     */
    NgAnimateScrollService.prototype.scrollTo = /**
     * @desc scrollTo Fn allows scrolling with animation.
     * @private
     * @param {?} element the 'element' that the scroll will happen on.
     * @param {?} to is the location to scroll to.
     * @param {?} duration is the length of the animation.
     * @param {?=} isContainer
     * @return {?}
     */
    function (element, to, duration, isContainer) {
        if (isContainer === void 0) { isContainer = false; }
        /** @type {?} */
        var increment = 20;
        /** @type {?} */
        var that = this;
        /** @type {?} */
        var start;
        /** @type {?} */
        var remaining;
        /** @type {?} */
        var currentTime = 0;
        /** @type {?} */
        var animateScroll;
        if (isContainer) {
            // for custom container element
            start = element.scrollTop;
        }
        else if (element.body.scrollTop > 0) {
            // for chrome
            start = element.body.scrollTop;
        }
        else if (element.documentElement.scrollTop > 0) {
            // for firefox
            start = element.documentElement.scrollTop;
        }
        else {
            start = 0;
        }
        remaining = to - start;
        animateScroll = (/**
         * @return {?}
         */
        function () {
            currentTime += increment;
            /** @type {?} */
            var val = that.easeInOut(currentTime, start, remaining, duration);
            if (isContainer) {
                element.scroll(0, val);
            }
            else {
                // to allow scroll function on different browsers both chrome and firefox
                top.window.scroll(0, val);
            }
            if (currentTime < duration) {
                setTimeout(animateScroll, increment);
            }
        });
        animateScroll();
    };
    /**
     * @desc easeInOut Fn creates the values necessary to create easeInOut animation.
     * @param currentTime is current time.
     * @param startTime is the starting time.
     * @param remainingTime is the time period in the value.
     * @param duration is the duration of the animation
     * @returns a number value to scroll to.
     */
    /**
     * @desc easeInOut Fn creates the values necessary to create easeInOut animation.
     * @private
     * @param {?} currentTime is current time.
     * @param {?} startTime is the starting time.
     * @param {?} remainingTime is the time period in the value.
     * @param {?} duration is the duration of the animation
     * @return {?} a number value to scroll to.
     */
    NgAnimateScrollService.prototype.easeInOut = /**
     * @desc easeInOut Fn creates the values necessary to create easeInOut animation.
     * @private
     * @param {?} currentTime is current time.
     * @param {?} startTime is the starting time.
     * @param {?} remainingTime is the time period in the value.
     * @param {?} duration is the duration of the animation
     * @return {?} a number value to scroll to.
     */
    function (currentTime, startTime, remainingTime, duration) {
        currentTime /= duration / 2;
        if (currentTime < 1) {
            return (remainingTime / 2) * currentTime * currentTime + startTime;
        }
        currentTime--;
        return ((-remainingTime / 2) * (currentTime * (currentTime - 2) - 1) + startTime);
    };
    NgAnimateScrollService.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"], args: [{
                    providedIn: "root"
                },] }
    ];
    /** @nocollapse */
    NgAnimateScrollService.ctorParameters = function () { return []; };
    /** @nocollapse */ NgAnimateScrollService.ngInjectableDef = Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["defineInjectable"])({ factory: function NgAnimateScrollService_Factory() { return new NgAnimateScrollService(); }, token: NgAnimateScrollService, providedIn: "root" });
    return NgAnimateScrollService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */



//# sourceMappingURL=ng-animate-scroll.js.map

/***/ })

}]);
//# sourceMappingURL=common.js.map